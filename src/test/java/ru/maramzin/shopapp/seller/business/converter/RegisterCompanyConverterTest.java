package ru.maramzin.shopapp.seller.business.converter;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.maramzin.shopapp.seller.business.converter.dto.CompanyConverter;
import ru.maramzin.shopapp.seller.business.dto.model.CompanyDto;
import ru.maramzin.shopapp.seller.business.dto.response.register.RegisterCompanyResponse;
import ru.maramzin.shopapp.seller.data.entity.Company;

@ExtendWith(MockitoExtension.class)
class RegisterCompanyConverterTest {

  @Mock
  private CompanyConverter companyConverter;

  @InjectMocks
  private RegisterCompanyConverter registerCompanyConverter;

  @Test
  void toCompanyResponse_workCorrectly() {
    Company company = Company.builder()
        .name("Roga and Copyta")
        .build();
    Mockito.when(companyConverter.toDto(company))
        .thenReturn(CompanyDto.builder()
            .name(company.getName())
            .build());

    RegisterCompanyResponse response = registerCompanyConverter.toCompanyResponse(company);
    Assertions.assertThat(response.getName()).isEqualTo(company.getName());
  }
}