package ru.maramzin.shopapp.seller.business.message;

public enum ExceptionMessage {
  ;
  public static final String COMPANY_NOT_FOUND_BY_NAME = "Company not found by name '%s'";
  public static final String SELLER_NOT_FOUND_BY_ID = "Seller not found by id '%s'";
}
