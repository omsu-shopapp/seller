package ru.maramzin.shopapp.seller.business.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.seller.business.converter.dto.CompanyConverter;
import ru.maramzin.shopapp.seller.business.dto.response.search.SearchCompanyResponse;
import ru.maramzin.shopapp.seller.data.entity.Company;

@Component
@RequiredArgsConstructor
public class SearchCompanyConverter {

  private final CompanyConverter companyConverter;

  public SearchCompanyResponse toResponse(Company company) {
    return new SearchCompanyResponse(companyConverter.toDto(company));
  }
}
