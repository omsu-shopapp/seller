package ru.maramzin.shopapp.seller.business.dto.response.search;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.seller.business.dto.model.CompanyDto;

@Data
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
public class SearchCompanyResponse extends CompanyDto {

  public SearchCompanyResponse(CompanyDto companyDto) {
    super(companyDto.toBuilder());
  }
}
