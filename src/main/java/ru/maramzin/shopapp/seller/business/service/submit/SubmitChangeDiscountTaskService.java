package ru.maramzin.shopapp.seller.business.service.submit;

import java.time.ZoneOffset;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.seller.business.converter.SubmitChangeDiscountTaskConverter;
import ru.maramzin.shopapp.seller.business.dto.request.ExecuteTaskRequest;
import ru.maramzin.shopapp.seller.business.dto.request.submit.SubmitChangeDiscountTaskRequest;
import ru.maramzin.shopapp.seller.business.dto.response.submit.SubmitChangeDiscountTaskResponse;
import ru.maramzin.shopapp.seller.business.exception.NotFoundException;
import ru.maramzin.shopapp.seller.business.message.ExceptionMessage;
import ru.maramzin.shopapp.seller.business.service.ExecuteTaskService;
import ru.maramzin.shopapp.seller.business.service.RequestProcessor;
import ru.maramzin.shopapp.seller.data.entity.BaseTask;
import ru.maramzin.shopapp.seller.data.entity.ChangeDiscountTask;
import ru.maramzin.shopapp.seller.data.entity.Seller;
import ru.maramzin.shopapp.seller.data.repository.ChangeDiscountTaskRepository;
import ru.maramzin.shopapp.seller.data.repository.SellerRepository;
import ru.maramzin.shopapp.seller.rest.client.StorageClient;

@Service
@RequiredArgsConstructor
public class SubmitChangeDiscountTaskService implements
    RequestProcessor<SubmitChangeDiscountTaskRequest, SubmitChangeDiscountTaskResponse> {

  private final SubmitChangeDiscountTaskConverter converter;
  private final ChangeDiscountTaskRepository changeDiscountTaskRepository;
  private final SellerRepository sellerRepository;
  private final ThreadPoolTaskScheduler taskScheduler;
  private final StorageClient storageClient;
  private final ExecuteTaskService executeTaskService;

  @Override
  @Transactional
  public SubmitChangeDiscountTaskResponse process(SubmitChangeDiscountTaskRequest request) {
    UUID sellerId = request.getSellerId();
    Seller seller = sellerRepository.findById(sellerId)
        .orElseThrow(() -> new NotFoundException(
            String.format(ExceptionMessage.SELLER_NOT_FOUND_BY_ID, sellerId)));

    ChangeDiscountTask task = converter.toChangeDiscountTask(request);
    task.setSeller(seller);
    changeDiscountTaskRepository.save(task);

    taskScheduler.schedule(() -> executeTaskService.process(getExecuteTask(changeDiscount(task), task)),
        task.getExecutionTime().toInstant(ZoneOffset.of("+6")));

    return converter.toResponse(task);
  }

  private Runnable changeDiscount(ChangeDiscountTask task) {
    return () -> storageClient.changePrice(task.getProductName(),
        task.getDiscount(), task.getSeller().getId());
  }

  private ExecuteTaskRequest getExecuteTask(Runnable runnable, BaseTask task) {
    return ExecuteTaskRequest.builder()
        .runnable(runnable)
        .task(task)
        .build();
  }
}
