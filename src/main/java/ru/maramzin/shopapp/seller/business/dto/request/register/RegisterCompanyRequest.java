package ru.maramzin.shopapp.seller.business.dto.request.register;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.maramzin.shopapp.seller.business.dto.model.AddressDto;

@Data
@NoArgsConstructor
public class RegisterCompanyRequest {

  private String name;
  private String phoneNumber;
  private String taxpayerId;
  private AddressDto address;
}
