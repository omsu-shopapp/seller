package ru.maramzin.shopapp.seller.business.exception;

import lombok.Getter;

import java.time.LocalDateTime;

@Getter
public class ExtendedException extends RuntimeException {

    private final ExceptionDto exceptionDto;

    public ExtendedException(String message) {
        this(new ExceptionDto(message, LocalDateTime.now()));
    }

    public ExtendedException(ExceptionDto exceptionDto) {
        super(exceptionDto.getErrorDescription());
        this.exceptionDto = exceptionDto;
    }
}
