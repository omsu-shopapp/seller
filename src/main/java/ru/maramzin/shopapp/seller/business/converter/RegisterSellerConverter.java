package ru.maramzin.shopapp.seller.business.converter;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.seller.business.converter.dto.SellerConverter;
import ru.maramzin.shopapp.seller.business.dto.request.register.RegisterSellerRequest;
import ru.maramzin.shopapp.seller.business.dto.response.register.RegisterSellerResponse;
import ru.maramzin.shopapp.seller.data.entity.Seller;

@Component
@RequiredArgsConstructor
public class RegisterSellerConverter {

  private final SellerConverter sellerConverter;

  public Seller toSeller(RegisterSellerRequest request) {
    return Seller.builder()
        .id(UUID.randomUUID())
        .email(request.getEmail())
        .phoneNumber(request.getPhoneNumber())
        .firstname(request.getFirstname())
        .surname(request.getSurname())
        .build();
  }

  public RegisterSellerResponse toResponse(Seller seller) {
    return new RegisterSellerResponse(sellerConverter.toDto(seller));
  }
}