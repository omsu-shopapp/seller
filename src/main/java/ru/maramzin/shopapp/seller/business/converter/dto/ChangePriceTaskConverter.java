package ru.maramzin.shopapp.seller.business.converter.dto;

import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.seller.business.dto.model.ChangePriceTaskDto;
import ru.maramzin.shopapp.seller.data.entity.ChangePriceTask;

@Component
public class ChangePriceTaskConverter implements DtoConverter<ChangePriceTask, ChangePriceTaskDto> {

  @Override
  public ChangePriceTaskDto toDto(ChangePriceTask entity) {
    return ChangePriceTaskDto.builder()
        .creationTime(entity.getCreationTime())
        .executionTime(entity.getExecutionTime())
        .sellerId(entity.getSeller().getId())
        .status(entity.getStatus())
        .productName(entity.getProductName())
        .price(entity.getPrice())
        .build();
  }
}
