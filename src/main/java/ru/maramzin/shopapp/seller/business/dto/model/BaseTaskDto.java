package ru.maramzin.shopapp.seller.business.dto.model;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.seller.data.entity.enumeration.TaskStatus;

@Data
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
public class BaseTaskDto {

  private LocalDateTime creationTime;
  private LocalDateTime executionTime;
  private TaskStatus status;
  private String productName;
  private UUID sellerId;
}
