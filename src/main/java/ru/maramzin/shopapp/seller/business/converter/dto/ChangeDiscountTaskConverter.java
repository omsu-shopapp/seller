package ru.maramzin.shopapp.seller.business.converter.dto;

import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.seller.business.dto.model.ChangeDiscountTaskDto;
import ru.maramzin.shopapp.seller.data.entity.ChangeDiscountTask;

@Component
public class ChangeDiscountTaskConverter implements DtoConverter<ChangeDiscountTask, ChangeDiscountTaskDto> {

  @Override
  public ChangeDiscountTaskDto toDto(ChangeDiscountTask entity) {
    return ChangeDiscountTaskDto.builder()
        .creationTime(entity.getCreationTime())
        .executionTime(entity.getExecutionTime())
        .sellerId(entity.getSeller().getId())
        .status(entity.getStatus())
        .productName(entity.getProductName())
        .discount(entity.getDiscount())
        .build();
  }
}
