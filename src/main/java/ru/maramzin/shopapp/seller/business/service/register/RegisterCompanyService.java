package ru.maramzin.shopapp.seller.business.service.register;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.seller.business.converter.RegisterCompanyConverter;
import ru.maramzin.shopapp.seller.business.dto.request.register.RegisterCompanyRequest;
import ru.maramzin.shopapp.seller.business.dto.response.register.RegisterCompanyResponse;
import ru.maramzin.shopapp.seller.business.service.RequestProcessor;
import ru.maramzin.shopapp.seller.data.entity.Company;
import ru.maramzin.shopapp.seller.data.repository.CompanyRepository;

@Service
@RequiredArgsConstructor
public class RegisterCompanyService implements
    RequestProcessor<RegisterCompanyRequest, RegisterCompanyResponse> {

  private final CompanyRepository companyRepository;
  private final RegisterCompanyConverter companyConverter;

  @Override
  @Transactional
  public RegisterCompanyResponse process(RegisterCompanyRequest request) {
    Company company = companyConverter.toCompany(request);
    companyRepository.save(company);
    return companyConverter.toCompanyResponse(company);
  }
}
