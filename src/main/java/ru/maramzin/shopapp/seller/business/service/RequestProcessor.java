package ru.maramzin.shopapp.seller.business.service;

public interface RequestProcessor<RQ, RS> {

  RS process(RQ request);
}
