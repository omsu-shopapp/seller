package ru.maramzin.shopapp.seller.business.converter;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.seller.business.converter.dto.CompanyConverter;
import ru.maramzin.shopapp.seller.business.dto.model.AddressDto;
import ru.maramzin.shopapp.seller.business.dto.request.register.RegisterCompanyRequest;
import ru.maramzin.shopapp.seller.business.dto.response.register.RegisterCompanyResponse;
import ru.maramzin.shopapp.seller.data.entity.Address;
import ru.maramzin.shopapp.seller.data.entity.Company;

@Component
@RequiredArgsConstructor
public class RegisterCompanyConverter {

  private final CompanyConverter companyConverter;

  public Company toCompany(RegisterCompanyRequest request) {
    return Company.builder()
        .id(UUID.randomUUID())
        .name(request.getName())
        .phoneNumber(request.getPhoneNumber())
        .taxpayerId(request.getTaxpayerId())
        .balance(0)
        .address(toAddress(request.getAddress()))
        .build();
  }

  public RegisterCompanyResponse toCompanyResponse(Company company) {
    return new RegisterCompanyResponse(companyConverter.toDto(company));
  }

  private Address toAddress(AddressDto addressDto) {
    return Address.builder()
        .id(UUID.randomUUID())
        .city(addressDto.getCity())
        .district(addressDto.getDistrict())
        .street(addressDto.getStreet())
        .house(addressDto.getHouse())
        .apartment(addressDto.getApartment())
        .zipcode(addressDto.getZipcode())
        .build();
  }
}
