package ru.maramzin.shopapp.seller.business.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.seller.business.dto.request.ExecuteTaskRequest;
import ru.maramzin.shopapp.seller.data.entity.BaseTask;
import ru.maramzin.shopapp.seller.data.entity.ChangeDiscountTask;
import ru.maramzin.shopapp.seller.data.entity.ChangePriceTask;
import ru.maramzin.shopapp.seller.data.entity.ReplenishmentTask;
import ru.maramzin.shopapp.seller.data.entity.enumeration.TaskStatus;
import ru.maramzin.shopapp.seller.data.repository.ChangeDiscountTaskRepository;
import ru.maramzin.shopapp.seller.data.repository.ChangePriceTaskRepository;
import ru.maramzin.shopapp.seller.data.repository.ReplenishmentTaskRepository;

@Service
@RequiredArgsConstructor
public class ExecuteTaskService implements RequestProcessor<ExecuteTaskRequest, Void>{

  private final ReplenishmentTaskRepository replenishmentTaskRepository;
  private final ChangePriceTaskRepository changePriceTaskRepository;
  private final ChangeDiscountTaskRepository changeDiscountTaskRepository;

  @Override
  @Transactional
  public Void process(ExecuteTaskRequest request) {
    BaseTask task = request.getTask();
    try {
      request.getRunnable().run();
      task.setStatus(TaskStatus.EXECUTED);
    } catch (Throwable throwable) {
      task.setStatus(TaskStatus.FAILED);
      task.setErrorDescription(throwable.getMessage());
    }

    if(task instanceof ReplenishmentTask replenishmentTask) {
      replenishmentTaskRepository.save(replenishmentTask);
    }

    if(task instanceof ChangePriceTask changePriceTask) {
      changePriceTaskRepository.save(changePriceTask);
    }

    if(task instanceof ChangeDiscountTask changeDiscountTask) {
      changeDiscountTaskRepository.save(changeDiscountTask);
    }

    return null;
  }
}
