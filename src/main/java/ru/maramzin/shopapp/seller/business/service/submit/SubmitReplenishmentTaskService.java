package ru.maramzin.shopapp.seller.business.service.submit;

import java.time.ZoneOffset;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.seller.business.converter.SubmitReplenishmentTaskConverter;
import ru.maramzin.shopapp.seller.business.dto.request.ExecuteTaskRequest;
import ru.maramzin.shopapp.seller.business.dto.request.submit.SubmitReplenishmentTaskRequest;
import ru.maramzin.shopapp.seller.business.dto.response.submit.SubmitReplenishmentTaskResponse;
import ru.maramzin.shopapp.seller.business.exception.NotFoundException;
import ru.maramzin.shopapp.seller.business.message.ExceptionMessage;
import ru.maramzin.shopapp.seller.business.service.ExecuteTaskService;
import ru.maramzin.shopapp.seller.business.service.RequestProcessor;
import ru.maramzin.shopapp.seller.data.entity.BaseTask;
import ru.maramzin.shopapp.seller.data.entity.ReplenishmentTask;
import ru.maramzin.shopapp.seller.data.entity.Seller;
import ru.maramzin.shopapp.seller.data.repository.ReplenishmentTaskRepository;
import ru.maramzin.shopapp.seller.data.repository.SellerRepository;
import ru.maramzin.shopapp.seller.rest.client.StorageClient;

@Service
@RequiredArgsConstructor
public class SubmitReplenishmentTaskService
    implements RequestProcessor<SubmitReplenishmentTaskRequest, SubmitReplenishmentTaskResponse> {

  private final SubmitReplenishmentTaskConverter converter;
  private final ReplenishmentTaskRepository replenishmentTaskRepository;
  private final SellerRepository sellerRepository;
  private final ThreadPoolTaskScheduler taskScheduler;
  private final StorageClient storageClient;
  private final ExecuteTaskService executeTaskService;

  @Override
  @Transactional
  public SubmitReplenishmentTaskResponse process(SubmitReplenishmentTaskRequest request) {
    UUID sellerId = request.getSellerId();
    Seller seller = sellerRepository.findById(sellerId)
        .orElseThrow(() -> new NotFoundException(
            String.format(ExceptionMessage.SELLER_NOT_FOUND_BY_ID, sellerId)));

    ReplenishmentTask task = converter.toReplenishmentTask(request);
    task.setSeller(seller);
    replenishmentTaskRepository.save(task);

    taskScheduler.schedule(() -> executeTaskService.process(getExecuteTask(replenish(task), task)),
        task.getExecutionTime().toInstant(ZoneOffset.of("+6")));

    return converter.toResponse(task);
  }

  private Runnable replenish(ReplenishmentTask task) {
    return () -> storageClient.replenish(task.getProductName(), task.getQuantity(), task.getSeller().getId());
  }

  private ExecuteTaskRequest getExecuteTask(Runnable runnable, BaseTask task) {
    return ExecuteTaskRequest.builder()
        .runnable(runnable)
        .task(task)
        .build();
  }
}
