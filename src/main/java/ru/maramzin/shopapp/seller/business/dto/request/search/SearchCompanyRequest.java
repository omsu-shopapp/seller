package ru.maramzin.shopapp.seller.business.dto.request.search;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SearchCompanyRequest {

  private String companyName;
}
