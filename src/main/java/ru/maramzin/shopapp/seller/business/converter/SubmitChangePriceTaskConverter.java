package ru.maramzin.shopapp.seller.business.converter;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.seller.business.converter.dto.ChangePriceTaskConverter;
import ru.maramzin.shopapp.seller.business.dto.request.submit.SubmitChangePriceTaskRequest;
import ru.maramzin.shopapp.seller.business.dto.response.submit.SubmitChangePriceTaskResponse;
import ru.maramzin.shopapp.seller.data.entity.ChangePriceTask;
import ru.maramzin.shopapp.seller.data.entity.enumeration.TaskStatus;

@Component
@RequiredArgsConstructor
public class SubmitChangePriceTaskConverter {

  private final ChangePriceTaskConverter changePriceTaskConverter;

  public ChangePriceTask toChangePriceTask(SubmitChangePriceTaskRequest request) {
    return ChangePriceTask.builder()
        .id(UUID.randomUUID())
        .creationTime(LocalDateTime.now())
        .executionTime(request.getExecutionTime())
        .status(TaskStatus.CREATED)
        .productName(request.getProductName())
        .price(request.getPrice())
        .build();
  }

  public SubmitChangePriceTaskResponse toResponse(ChangePriceTask task) {
    return new SubmitChangePriceTaskResponse(changePriceTaskConverter.toDto(task));
  }
}
