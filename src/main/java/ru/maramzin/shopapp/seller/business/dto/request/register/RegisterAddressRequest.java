package ru.maramzin.shopapp.seller.business.dto.request.register;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.seller.business.dto.model.AddressDto;

@Data
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
public class RegisterAddressRequest extends AddressDto {
}