package ru.maramzin.shopapp.seller.business.dto.response.submit;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.seller.business.dto.model.ReplenishmentTaskDto;

@Data
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
public class SubmitReplenishmentTaskResponse extends ReplenishmentTaskDto {

  public SubmitReplenishmentTaskResponse(ReplenishmentTaskDto taskDto) {
    super(taskDto.toBuilder());
  }
}
