package ru.maramzin.shopapp.seller.business.converter;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.seller.business.converter.dto.ChangeDiscountTaskConverter;
import ru.maramzin.shopapp.seller.business.dto.request.submit.SubmitChangeDiscountTaskRequest;
import ru.maramzin.shopapp.seller.business.dto.response.submit.SubmitChangeDiscountTaskResponse;
import ru.maramzin.shopapp.seller.data.entity.ChangeDiscountTask;
import ru.maramzin.shopapp.seller.data.entity.enumeration.TaskStatus;

@Component
@RequiredArgsConstructor
public class SubmitChangeDiscountTaskConverter {

  private final ChangeDiscountTaskConverter changeDiscountTaskConverter;

  public ChangeDiscountTask toChangeDiscountTask(SubmitChangeDiscountTaskRequest request) {
    return ChangeDiscountTask.builder()
        .id(UUID.randomUUID())
        .creationTime(LocalDateTime.now())
        .executionTime(request.getExecutionTime())
        .status(TaskStatus.CREATED)
        .productName(request.getProductName())
        .discount(request.getDiscount())
        .build();
  }

  public SubmitChangeDiscountTaskResponse toResponse(ChangeDiscountTask task) {
    return new SubmitChangeDiscountTaskResponse(changeDiscountTaskConverter.toDto(task));
  }
}
