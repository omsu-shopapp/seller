package ru.maramzin.shopapp.seller.business.converter.dto;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.seller.business.dto.model.CompanyDto;
import ru.maramzin.shopapp.seller.data.entity.Company;

@Component
@RequiredArgsConstructor
public class CompanyConverter implements DtoConverter<Company, CompanyDto> {

  private final AddressConverter addressConverter;

  @Override
  public CompanyDto toDto(Company entity) {
    return CompanyDto.builder()
        .name(entity.getName())
        .phoneNumber(entity.getPhoneNumber())
        .taxpayerId(entity.getTaxpayerId())
        .balance(entity.getBalance())
        .address(addressConverter.toDto(entity.getAddress()))
        .build();
  }
}
