package ru.maramzin.shopapp.seller.business.dto.request.submit;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SubmitChangeDiscountTaskRequest {

  private LocalDateTime executionTime;
  private String productName;
  private UUID sellerId;
  private Integer discount;
}
