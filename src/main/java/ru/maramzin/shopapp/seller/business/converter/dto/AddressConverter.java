package ru.maramzin.shopapp.seller.business.converter.dto;

import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.seller.business.dto.model.AddressDto;
import ru.maramzin.shopapp.seller.data.entity.Address;

@Component
public class AddressConverter implements DtoConverter<Address, AddressDto> {

  @Override
  public AddressDto toDto(Address entity) {
    return AddressDto.builder()
        .city(entity.getCity())
        .district(entity.getDistrict())
        .street(entity.getStreet())
        .house(entity.getHouse())
        .apartment(entity.getApartment())
        .zipcode(entity.getZipcode())
        .build();
  }
}
