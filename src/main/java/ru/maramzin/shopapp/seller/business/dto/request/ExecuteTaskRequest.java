package ru.maramzin.shopapp.seller.business.dto.request;

import lombok.Builder;
import lombok.Data;
import ru.maramzin.shopapp.seller.data.entity.BaseTask;

@Data
@Builder
public class ExecuteTaskRequest {

  private BaseTask task;
  private Runnable runnable;
}