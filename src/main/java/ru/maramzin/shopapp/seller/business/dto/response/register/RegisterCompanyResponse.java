package ru.maramzin.shopapp.seller.business.dto.response.register;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.seller.business.dto.model.CompanyDto;

@Data
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
public class RegisterCompanyResponse extends CompanyDto {

  public RegisterCompanyResponse(CompanyDto companyDto) {
    super(companyDto.toBuilder());
  }
}
