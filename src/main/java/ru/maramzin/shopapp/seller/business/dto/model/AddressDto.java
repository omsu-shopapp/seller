package ru.maramzin.shopapp.seller.business.dto.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
public class AddressDto {

  private String city;
  private String district;
  private String street;
  private String house;
  private Integer apartment;
  private String zipcode;
}
