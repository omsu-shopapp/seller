package ru.maramzin.shopapp.seller.business.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.seller.data.entity.Company;
import ru.maramzin.shopapp.seller.data.entity.Seller;
import ru.maramzin.shopapp.seller.data.repository.CompanyRepository;
import ru.maramzin.shopapp.seller.data.repository.SellerRepository;
import ru.maramzin.shopapp.seller.rest.client.StorageClient;
import ru.maramzin.shopapp.seller.rest.dto.CashOutResponse;

@Slf4j
@Service
@RequiredArgsConstructor
public class ReceiveCashOutService {

  private final StorageClient storageClient;
  private final SellerRepository sellerRepository;
  private final CompanyRepository companyRepository;

  @Transactional
  public void process() {
    log.info("Start processing request to receive cash out at {}", LocalDateTime.now());
    List<CashOutResponse> cashOutInfos = storageClient.cashOut();
    cashOutInfos.forEach(cashOutInfo -> {
      Optional<Seller> optionalSeller = sellerRepository.findById(cashOutInfo.getSellerId());
      if(optionalSeller.isPresent()) {
        Seller seller = optionalSeller.get();
        Company company = seller.getCompany();
        company.setBalance(company.getBalance() + cashOutInfo.getTotalCashOut());
        companyRepository.save(company);
      }
    });
  }
}
