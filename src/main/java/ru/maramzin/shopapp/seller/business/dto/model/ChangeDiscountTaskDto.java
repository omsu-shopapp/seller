package ru.maramzin.shopapp.seller.business.dto.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ChangeDiscountTaskDto extends BaseTaskDto {

  private Integer discount;
}
