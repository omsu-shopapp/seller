package ru.maramzin.shopapp.seller.business.service.search;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.seller.business.converter.SearchCompanyConverter;
import ru.maramzin.shopapp.seller.business.dto.request.search.SearchCompanyRequest;
import ru.maramzin.shopapp.seller.business.dto.response.search.SearchCompanyResponse;
import ru.maramzin.shopapp.seller.business.exception.NotFoundException;
import ru.maramzin.shopapp.seller.business.message.ExceptionMessage;
import ru.maramzin.shopapp.seller.business.service.RequestProcessor;
import ru.maramzin.shopapp.seller.data.entity.Company;
import ru.maramzin.shopapp.seller.data.repository.CompanyRepository;

@Service
@RequiredArgsConstructor
public class SearchCompanyService implements RequestProcessor<SearchCompanyRequest, SearchCompanyResponse> {

  private final CompanyRepository companyRepository;
  private final SearchCompanyConverter converter;

  @Override
  @Transactional
  public SearchCompanyResponse process(SearchCompanyRequest request) {
    String companyName = request.getCompanyName();
    Company company = companyRepository.findByName(companyName)
        .orElseThrow(() -> new NotFoundException(
            String.format(ExceptionMessage.COMPANY_NOT_FOUND_BY_NAME, companyName)));

    return converter.toResponse(company);
  }
}
