package ru.maramzin.shopapp.seller.business.converter.dto;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.seller.business.dto.model.SellerDto;
import ru.maramzin.shopapp.seller.data.entity.Seller;

@Component
@RequiredArgsConstructor
public class SellerConverter implements DtoConverter<Seller, SellerDto> {

  private final CompanyConverter companyConverter;

  @Override
  public SellerDto toDto(Seller entity) {
    return SellerDto.builder()
        .firstname(entity.getFirstname())
        .surname(entity.getSurname())
        .email(entity.getEmail())
        .phoneNumber(entity.getPhoneNumber())
        .company(companyConverter.toDto(entity.getCompany()))
        .build();
  }
}
