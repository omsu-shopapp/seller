package ru.maramzin.shopapp.seller.business.dto.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
public class SellerDto {

  private String firstname;
  private String surname;
  private String email;
  private String phoneNumber;
  private CompanyDto company;
}
