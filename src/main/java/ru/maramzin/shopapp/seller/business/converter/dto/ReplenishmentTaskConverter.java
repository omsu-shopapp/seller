package ru.maramzin.shopapp.seller.business.converter.dto;

import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.seller.business.dto.model.ReplenishmentTaskDto;
import ru.maramzin.shopapp.seller.data.entity.ReplenishmentTask;

@Component
public class ReplenishmentTaskConverter implements DtoConverter<ReplenishmentTask, ReplenishmentTaskDto> {

  @Override
  public ReplenishmentTaskDto toDto(ReplenishmentTask entity) {
    return ReplenishmentTaskDto.builder()
        .creationTime(entity.getCreationTime())
        .executionTime(entity.getExecutionTime())
        .sellerId(entity.getSeller().getId())
        .status(entity.getStatus())
        .productName(entity.getProductName())
        .quantity(entity.getQuantity())
        .build();
  }
}
