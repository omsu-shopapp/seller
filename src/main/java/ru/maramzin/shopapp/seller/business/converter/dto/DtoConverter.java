package ru.maramzin.shopapp.seller.business.converter.dto;

public interface DtoConverter<E, D> {

  D toDto(E entity);
}
