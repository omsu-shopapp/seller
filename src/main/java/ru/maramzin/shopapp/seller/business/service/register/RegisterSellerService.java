package ru.maramzin.shopapp.seller.business.service.register;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.seller.business.converter.RegisterSellerConverter;
import ru.maramzin.shopapp.seller.business.dto.request.register.RegisterSellerRequest;
import ru.maramzin.shopapp.seller.business.dto.response.register.RegisterSellerResponse;
import ru.maramzin.shopapp.seller.business.exception.NotFoundException;
import ru.maramzin.shopapp.seller.business.message.ExceptionMessage;
import ru.maramzin.shopapp.seller.business.service.RequestProcessor;
import ru.maramzin.shopapp.seller.data.entity.Company;
import ru.maramzin.shopapp.seller.data.entity.Seller;
import ru.maramzin.shopapp.seller.data.repository.CompanyRepository;
import ru.maramzin.shopapp.seller.data.repository.SellerRepository;

@Service
@RequiredArgsConstructor
public class RegisterSellerService implements
    RequestProcessor<RegisterSellerRequest, RegisterSellerResponse> {

  private final SellerRepository sellerRepository;
  private final CompanyRepository companyRepository;
  private final RegisterSellerConverter converter;

  @Override
  @Transactional
  public RegisterSellerResponse process(RegisterSellerRequest request) {
    String companyName = request.getCompanyName();
    Company company = companyRepository.findByName(companyName)
        .orElseThrow(() -> new NotFoundException(
            String.format(ExceptionMessage.COMPANY_NOT_FOUND_BY_NAME, companyName)));

    Seller seller = converter.toSeller(request);
    seller.setCompany(company);
    sellerRepository.save(seller);

    return converter.toResponse(seller);
  }
}
