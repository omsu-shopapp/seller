package ru.maramzin.shopapp.seller.business.dto.response.register;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.seller.business.dto.model.SellerDto;

@Data
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
public class RegisterSellerResponse extends SellerDto {

  public RegisterSellerResponse(SellerDto sellerDto) {
    super(sellerDto.toBuilder());
  }
}
