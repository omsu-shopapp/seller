package ru.maramzin.shopapp.seller.business.dto.request.register;

import lombok.Data;

@Data
public class RegisterSellerRequest {

  private String email;
  private String phoneNumber;
  private String firstname;
  private String surname;
  private String companyName;
}
