package ru.maramzin.shopapp.seller.business.dto.response.submit;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.seller.business.dto.model.ChangeDiscountTaskDto;

@Data
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
public class SubmitChangeDiscountTaskResponse extends ChangeDiscountTaskDto {

  public SubmitChangeDiscountTaskResponse(ChangeDiscountTaskDto taskDto) {
    super(taskDto.toBuilder());
  }
}
