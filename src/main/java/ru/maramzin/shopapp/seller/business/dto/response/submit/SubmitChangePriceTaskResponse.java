package ru.maramzin.shopapp.seller.business.dto.response.submit;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.seller.business.dto.model.ChangePriceTaskDto;

@Data
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
public class SubmitChangePriceTaskResponse extends ChangePriceTaskDto {

  public SubmitChangePriceTaskResponse(ChangePriceTaskDto taskDto) {
    super(taskDto.toBuilder());
  }
}
