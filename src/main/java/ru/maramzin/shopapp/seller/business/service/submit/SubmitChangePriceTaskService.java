package ru.maramzin.shopapp.seller.business.service.submit;

import java.time.ZoneOffset;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import ru.maramzin.shopapp.seller.business.converter.SubmitChangePriceTaskConverter;
import ru.maramzin.shopapp.seller.business.dto.request.ExecuteTaskRequest;
import ru.maramzin.shopapp.seller.business.dto.request.submit.SubmitChangePriceTaskRequest;
import ru.maramzin.shopapp.seller.business.dto.response.submit.SubmitChangePriceTaskResponse;
import ru.maramzin.shopapp.seller.business.exception.NotFoundException;
import ru.maramzin.shopapp.seller.business.message.ExceptionMessage;
import ru.maramzin.shopapp.seller.business.service.ExecuteTaskService;
import ru.maramzin.shopapp.seller.business.service.RequestProcessor;
import ru.maramzin.shopapp.seller.data.entity.BaseTask;
import ru.maramzin.shopapp.seller.data.entity.ChangePriceTask;
import ru.maramzin.shopapp.seller.data.entity.Seller;
import ru.maramzin.shopapp.seller.data.repository.ChangePriceTaskRepository;
import ru.maramzin.shopapp.seller.data.repository.SellerRepository;
import ru.maramzin.shopapp.seller.rest.client.StorageClient;

@Service
@RequiredArgsConstructor
public class SubmitChangePriceTaskService implements
    RequestProcessor<SubmitChangePriceTaskRequest, SubmitChangePriceTaskResponse> {

  private final SubmitChangePriceTaskConverter converter;
  private final ChangePriceTaskRepository changePriceTaskRepository;
  private final SellerRepository sellerRepository;
  private final ThreadPoolTaskScheduler taskScheduler;
  private final StorageClient storageClient;
  private final ExecuteTaskService executeTaskService;

  @Override
  @Transactional
  public SubmitChangePriceTaskResponse process(@Validated SubmitChangePriceTaskRequest request) {
    UUID sellerId = request.getSellerId();
    Seller seller = sellerRepository.findById(sellerId)
        .orElseThrow(() -> new NotFoundException(
            String.format(ExceptionMessage.SELLER_NOT_FOUND_BY_ID, sellerId)));

    ChangePriceTask task = converter.toChangePriceTask(request);
    task.setSeller(seller);
    changePriceTaskRepository.save(task);

    taskScheduler.schedule(() -> executeTaskService.process(getExecuteTask(changePrice(task), task)),
        task.getExecutionTime().toInstant(ZoneOffset.of("+6")));

    return converter.toResponse(task);
  }

  private Runnable changePrice(ChangePriceTask task) {
    return () -> storageClient.changePrice(task.getProductName(), task.getPrice(), task.getSeller().getId());
  }

  private ExecuteTaskRequest getExecuteTask(Runnable runnable, BaseTask task) {
    return ExecuteTaskRequest.builder()
        .runnable(runnable)
        .task(task)
        .build();
  }
}
