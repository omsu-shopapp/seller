package ru.maramzin.shopapp.seller.business.dto.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
public class CompanyDto {

  private String name;
  private String phoneNumber;
  private Integer balance;
  private String taxpayerId;
  private AddressDto address;
}
