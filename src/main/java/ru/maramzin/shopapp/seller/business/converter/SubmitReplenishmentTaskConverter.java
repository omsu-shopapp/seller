package ru.maramzin.shopapp.seller.business.converter;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.seller.business.converter.dto.ReplenishmentTaskConverter;
import ru.maramzin.shopapp.seller.business.dto.request.submit.SubmitReplenishmentTaskRequest;
import ru.maramzin.shopapp.seller.business.dto.response.submit.SubmitReplenishmentTaskResponse;
import ru.maramzin.shopapp.seller.data.entity.ReplenishmentTask;
import ru.maramzin.shopapp.seller.data.entity.enumeration.TaskStatus;

@Component
@RequiredArgsConstructor
public class SubmitReplenishmentTaskConverter {

  private final ReplenishmentTaskConverter replenishmentTaskConverter;

  public ReplenishmentTask toReplenishmentTask(SubmitReplenishmentTaskRequest request) {
    return ReplenishmentTask.builder()
        .id(UUID.randomUUID())
        .creationTime(LocalDateTime.now())
        .executionTime(request.getExecutionTime())
        .status(TaskStatus.CREATED)
        .productName(request.getProductName())
        .quantity(request.getQuantity())
        .build();
  }

  public SubmitReplenishmentTaskResponse toResponse(ReplenishmentTask task) {
    return new SubmitReplenishmentTaskResponse(replenishmentTaskConverter.toDto(task));
  }
}
