package ru.maramzin.shopapp.seller.trigger.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.maramzin.shopapp.seller.business.dto.request.search.SearchCompanyRequest;
import ru.maramzin.shopapp.seller.business.dto.response.search.SearchCompanyResponse;
import ru.maramzin.shopapp.seller.business.service.search.SearchCompanyService;

@RestController
@RequestMapping("/find")
@RequiredArgsConstructor
public class SearchController {

  private final SearchCompanyService searchCompanyService;

  @GetMapping("/company")
  public SearchCompanyResponse searchCompanyByName(@RequestParam String companyName) {
    return searchCompanyService.process(new SearchCompanyRequest(companyName));
  }
}
