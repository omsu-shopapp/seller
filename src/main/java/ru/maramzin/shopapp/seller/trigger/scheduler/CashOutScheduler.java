package ru.maramzin.shopapp.seller.trigger.scheduler;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.seller.business.service.ReceiveCashOutService;

@Component
@RequiredArgsConstructor
public class CashOutScheduler {

  private final ReceiveCashOutService receiveCashOutService;

  @Scheduled(cron = "0 0 0 * * ?")
  public void cashOut() {
    receiveCashOutService.process();
  }
}
