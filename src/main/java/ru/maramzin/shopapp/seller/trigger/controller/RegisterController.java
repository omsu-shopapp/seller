package ru.maramzin.shopapp.seller.trigger.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.maramzin.shopapp.seller.business.dto.request.register.RegisterCompanyRequest;
import ru.maramzin.shopapp.seller.business.dto.request.register.RegisterSellerRequest;
import ru.maramzin.shopapp.seller.business.dto.response.register.RegisterCompanyResponse;
import ru.maramzin.shopapp.seller.business.dto.response.register.RegisterSellerResponse;
import ru.maramzin.shopapp.seller.business.service.register.RegisterCompanyService;
import ru.maramzin.shopapp.seller.business.service.register.RegisterSellerService;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/register")
public class RegisterController {

  private final RegisterCompanyService registerCompanyService;
  private final RegisterSellerService registerSellerService;

  @PutMapping("/company")
  public RegisterCompanyResponse registerCompany(@RequestBody RegisterCompanyRequest request) {
    log.info("Rest request to register company: {}", request);
    return registerCompanyService.process(request);
  }

  @PutMapping("/seller")
  public RegisterSellerResponse registerSeller(@RequestBody RegisterSellerRequest request) {
    log.info("Rest request to register company: {}", request);
    return registerSellerService.process(request);
  }
}
