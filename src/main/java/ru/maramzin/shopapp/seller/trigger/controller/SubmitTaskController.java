package ru.maramzin.shopapp.seller.trigger.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.maramzin.shopapp.seller.business.dto.request.submit.SubmitChangeDiscountTaskRequest;
import ru.maramzin.shopapp.seller.business.dto.request.submit.SubmitChangePriceTaskRequest;
import ru.maramzin.shopapp.seller.business.dto.request.submit.SubmitReplenishmentTaskRequest;
import ru.maramzin.shopapp.seller.business.dto.response.submit.SubmitChangeDiscountTaskResponse;
import ru.maramzin.shopapp.seller.business.dto.response.submit.SubmitChangePriceTaskResponse;
import ru.maramzin.shopapp.seller.business.dto.response.submit.SubmitReplenishmentTaskResponse;
import ru.maramzin.shopapp.seller.business.service.submit.SubmitChangeDiscountTaskService;
import ru.maramzin.shopapp.seller.business.service.submit.SubmitChangePriceTaskService;
import ru.maramzin.shopapp.seller.business.service.submit.SubmitReplenishmentTaskService;

@RestController
@RequestMapping("/submit/task")
@RequiredArgsConstructor
public class SubmitTaskController {

  private final SubmitReplenishmentTaskService submitReplenishmentTaskService;
  private final SubmitChangePriceTaskService submitChangePriceTaskService;
  private final SubmitChangeDiscountTaskService submitChangeDiscountTaskService;

  @PutMapping("/replenishment")
  public SubmitReplenishmentTaskResponse submitReplenishmentTask(
      @RequestBody SubmitReplenishmentTaskRequest request
  ) {
    return submitReplenishmentTaskService.process(request);
  }

  @PutMapping("/change/price")
  public SubmitChangePriceTaskResponse submitChangePriceTask(
      @RequestBody SubmitChangePriceTaskRequest request
  ) {
    return submitChangePriceTaskService.process(request);
  }

  @PutMapping("/change/discount")
  public SubmitChangeDiscountTaskResponse submitChangeDiscountTask(
      @RequestBody SubmitChangeDiscountTaskRequest request
  ) {
    return submitChangeDiscountTaskService.process(request);
  }
}
