package ru.maramzin.shopapp.seller.trigger.controller.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.maramzin.shopapp.seller.business.exception.NotFoundException;
import ru.maramzin.shopapp.seller.business.exception.ExceptionDto;

@ControllerAdvice
public class RestExceptionHandler {

  @ExceptionHandler(NotFoundException.class)
  public ResponseEntity<ExceptionDto> handleNotFoundException(NotFoundException cause) {
    return new ResponseEntity<>(cause.getExceptionDto(), HttpStatus.NOT_FOUND);
  }
}
