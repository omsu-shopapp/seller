package ru.maramzin.shopapp.seller.trigger.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.maramzin.shopapp.seller.business.service.ReceiveCashOutService;

@RestController
@RequiredArgsConstructor
public class CashOutController {

  private final ReceiveCashOutService receiveCashOutService;

  @PostMapping("/cashOut")
  public void cashOut() {
    receiveCashOutService.process();
  }
}
