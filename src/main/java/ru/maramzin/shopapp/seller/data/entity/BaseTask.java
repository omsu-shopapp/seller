package ru.maramzin.shopapp.seller.data.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MappedSuperclass;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.maramzin.shopapp.seller.data.entity.enumeration.TaskStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@MappedSuperclass
public class BaseTask {

  @Id
  private UUID id;
  private LocalDateTime creationTime;
  private LocalDateTime executionTime;

  @Enumerated(EnumType.STRING)
  private TaskStatus status;

  private String productName;

  private String errorDescription;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "seller_id")
  private Seller seller;
}
