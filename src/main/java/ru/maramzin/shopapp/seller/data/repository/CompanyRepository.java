package ru.maramzin.shopapp.seller.data.repository;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.maramzin.shopapp.seller.data.entity.Company;

public interface CompanyRepository extends JpaRepository<Company, UUID> {

  Optional<Company> findByName(String name);
}
