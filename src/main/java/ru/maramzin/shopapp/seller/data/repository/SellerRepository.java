package ru.maramzin.shopapp.seller.data.repository;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.maramzin.shopapp.seller.data.entity.Seller;

public interface SellerRepository extends JpaRepository<Seller, UUID> {

}
