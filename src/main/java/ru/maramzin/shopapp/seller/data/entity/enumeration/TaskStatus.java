package ru.maramzin.shopapp.seller.data.entity.enumeration;

public enum TaskStatus {

  CREATED, EXECUTED, FAILED
}
