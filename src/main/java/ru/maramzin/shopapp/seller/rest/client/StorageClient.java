package ru.maramzin.shopapp.seller.rest.client;

import java.util.List;
import java.util.UUID;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.maramzin.shopapp.seller.config.FeignClientConfig;
import ru.maramzin.shopapp.seller.rest.dto.CashOutResponse;
import ru.maramzin.shopapp.seller.rest.dto.ProductInfoResponse;

@FeignClient(value = "shopapp-storage", url = "${rest.storage.url}", configuration = FeignClientConfig.class)
public interface StorageClient {

  @PostMapping("/api/v1/product/replenish")
  ProductInfoResponse replenish(
      @RequestParam String productName,
      @RequestParam Integer quantity,
      @RequestParam UUID sellerId);

  @PostMapping("/api/v1/product/change/price")
  ProductInfoResponse changePrice(
      @RequestParam String productName,
      @RequestParam Integer price,
      @RequestParam UUID sellerId);

  @PostMapping("/api/v1/product/change/price")
  ProductInfoResponse changeDiscount(
      @RequestParam String productName,
      @RequestParam Integer discount,
      @RequestParam UUID sellerId);

  @PostMapping("/api/v1/histories/selling/cashOut")
  List<CashOutResponse> cashOut();
}
