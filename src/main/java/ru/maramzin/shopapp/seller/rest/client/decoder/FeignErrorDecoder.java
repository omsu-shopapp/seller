package ru.maramzin.shopapp.seller.rest.client.decoder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import ru.maramzin.shopapp.seller.business.exception.NotFoundException;
import ru.maramzin.shopapp.seller.business.exception.StateException;
import ru.maramzin.shopapp.seller.business.exception.ValidationException;
import ru.maramzin.shopapp.seller.business.exception.ExceptionDto;
import ru.maramzin.shopapp.seller.rest.client.exception.StorageException;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class FeignErrorDecoder implements ErrorDecoder {

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Decodes feign client error response.
     *
     * @param methodKey configKey of the java method that invoked the request
     * @param response  response
     * @return {@link Exception}
     */
    @Override
    public Exception decode(String methodKey, Response response) {
        Stream<String> lines;

        try (var input = new BufferedReader(response.body()
                .asReader(StandardCharsets.UTF_8))
                .lines()) {
            lines = input;

            if (Objects.nonNull(lines)) {
                var content = lines
                        .collect(Collectors.joining("\n"));
                if (HttpStatus.BAD_REQUEST.value() == response.status()) {
                    var exceptionDto = objectMapper.readValue(content, ExceptionDto.class);
                    var timestamp = Optional.ofNullable(exceptionDto.getTimestamp())
                            .orElseGet(LocalDateTime::now);
                    exceptionDto.setTimestamp(timestamp);
                    return new ValidationException(exceptionDto);
                } else if (HttpStatus.NOT_FOUND.value() == response.status()) {
                    var exceptionDto = objectMapper.readValue(content, ExceptionDto.class);
                    return new NotFoundException(exceptionDto);
                } else if (HttpStatus.CONFLICT.value() == response.status()) {
                    var exceptionDto = objectMapper.readValue(content, ExceptionDto.class);
                    return new StateException(exceptionDto);
                }
            }
        } catch (JsonProcessingException ex) {
            log.error("Error occurred while json parsing:", ex);
            throw new StorageException(ex);
        } catch (IOException ex) {
            log.error("Error occurred while reading response:", ex);
            throw new StorageException(ex);
        }

        return new Exception(response.reason());
    }
}
