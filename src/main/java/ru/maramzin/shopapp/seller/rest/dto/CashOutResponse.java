package ru.maramzin.shopapp.seller.rest.dto;

import java.util.UUID;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CashOutResponse {

  private UUID sellerId;
  private Integer totalCashOut;
}
