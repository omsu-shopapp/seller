package ru.maramzin.shopapp.seller.rest.dto.enumeration;

/**
 * The enum Clothes category.
 */
public enum ClothesCategory {

  /**
   * Dress clothes' category.
   */
  DRESS,
  /**
   * Shoes clothes' category.
   */
  SHOES,
  /**
   * Shirt clothes' category.
   */
  SHIRT,
  /**
   * Hat clothes' category.
   */
  HAT,
  /**
   * Underwear clothes' category.
   */
  UNDERWEAR,
  /**
   * Jacket clothes' category.
   */
  JACKET
}
