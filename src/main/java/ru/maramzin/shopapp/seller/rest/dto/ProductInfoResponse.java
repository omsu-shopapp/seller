package ru.maramzin.shopapp.seller.rest.dto;

import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import ru.maramzin.shopapp.seller.rest.dto.enumeration.ClothesCategory;
import ru.maramzin.shopapp.seller.rest.dto.enumeration.SeasonCategory;

/**
 * The type Product info response.
 */
@Data
@Builder
public class ProductInfoResponse {

  private final UUID id;
  private final String name;
  private final String description;
  private final Integer cost;
  private final Integer discount;
  private final Integer quantity;
  private final ClothesCategory clothesCategory;
  private final SeasonCategory seasonCategory;
  private final UUID sellerId;
}
