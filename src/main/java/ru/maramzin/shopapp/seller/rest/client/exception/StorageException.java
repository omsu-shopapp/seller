package ru.maramzin.shopapp.seller.rest.client.exception;

public class StorageException extends RuntimeException {

    public StorageException(Throwable cause) {
        super(cause);
    }
}
