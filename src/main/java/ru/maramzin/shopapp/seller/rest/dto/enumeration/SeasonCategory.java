package ru.maramzin.shopapp.seller.rest.dto.enumeration;

/**
 * The enum Season category.
 */
public enum SeasonCategory {

  /**
   * Winter season category.
   */
  WINTER,
  /**
   * Spring season category.
   */
  SPRING,
  /**
   * Summer season category.
   */
  SUMMER,
  /**
   * Autumn season category.
   */
  AUTUMN
}
