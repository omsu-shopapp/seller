package ru.maramzin.shopapp.seller.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.maramzin.shopapp.seller.rest.client.decoder.FeignErrorDecoder;

@Configuration
public class FeignClientConfig {

    @Bean
    public FeignErrorDecoder errorDecoder() {
        return new FeignErrorDecoder();
    }
}
